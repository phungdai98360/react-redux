import React, { Component } from 'react';
import * as action from './../action/index';
import _ from 'lodash';
import {connect} from 'react-redux';
class Search extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      keyword :''
    }
  }
  onChange=(event)=>
  {
   var target=event.target;
   var name=target.name;
   var value=target.value;
   this.setState({
     [name]: value
   });
  }
  onSearch=()=>
  {
    this.props.onSearch(this.state.keyword);
  }
  render() {
    var {keyword}=this.state;
    
    
    return (
     
     <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <div className="input-group">
            
            <input
              type="text"
              name="keyword" 
              className="form-control"
              value={keyword}
              placeholder="Nhập từ khóa..."
              onChange={this.onChange}
            />
            
            <span className="input-group-btn">
                <button type="button"
                 className="btn btn-default"
                 onClick={this.onSearch}
                 >
                <span className="fa fa-search mr-5"/>
                 Search
                 </button>
            </span>
            
            
         </div>
     </div>
     
      
    );
  }
}
const mapStateToProps = state => {
  return {
    
  }
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSearch: (Keyword) => {
      dispatch(action.Search(Keyword))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Search);
