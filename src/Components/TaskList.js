import React, { Component } from 'react';
import TaskItem from "./TaskItem";
import {connect} from 'react-redux';
import * as action from './../action/index';
import _ from 'lodash';
class TaskList extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      Filtername:'',
      FilterStatus:-1
    }
  }
  onChange=(event)=>
  {
    var target=event.target;
    var name=target.name;
    var value=target.value;
    var Filter={
      name:name==='Filtername'?value:this.state.Filtername,
      status:name==='FilterStatus'?value:this.state.FilterStatus
    };
    this.props.onFilterTable(Filter);
    this.setState({
      [name]:value
    });
    //console.log(this.state);
  }
  render() {
    
    var {Tasks,Filter,keyword,SortBy}=this.props;
   
    if(Filter.name)
    {
      Tasks=_.filter(Tasks,(task)=>{
        return task.name.toLowerCase().indexOf(Filter.name.toLowerCase())!==-1;
      });
    }
    
      Tasks=_.filter(Tasks,(task)=>{
        if(Filter.status===-1)
        {
          return task;
        }
        else
        {
          return task.status===(Filter.status===1?true:false);
        }
      });
      if(keyword)
    {
      Tasks=_.filter(Tasks,(task)=>
      {
        return task.name.toLowerCase().indexOf(keyword.toLowerCase())!==-1;
      });
    }
    if(SortBy.By==='name')
    {
      Tasks.sort((a,b)=>{
        if(a.name>b.name)
        {
          return SortBy.Value;
        }
        else if(a.name<b.name)
        {
          return -SortBy.Value;
        }
      });
    }
    else 
    {
      Tasks.sort((a,b)=>{
        if(a.status>b.status)
        {
          return -SortBy.Value;
        }
        else if(a.status<b.status)
        {
          return SortBy.Value;
        }
      });
    }
    var {Filtername,FilterStatus}=this.state;
    var Element=Tasks.map((Task,index)=>{
      return <TaskItem
      key={Task.id}
      index={index}
      Task={Task}
      />
    });

    

    return (
      
      <table className="table table-bordered table-hover">
          <thead>
              <tr>
                  <th className="text-center">STT</th>
                  <th className="text-center">Name</th>
                  <th className="text-center">Status</th>
                  <th className="text-center">Action</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td></td>
                  <td>
                  <input className="form-control"
                  name="Filtername"
                  value={Filtername}
                  onChange={this.onChange}
                  />
                  </td>
                  <td>
                    <select className="form-control"

                    name="FilterStatus"
                    value={FilterStatus}
                    onChange={this.onChange}
                    >
                    <option value="-1">Tât cả</option>
                    <option value="0">Ẩn</option>
                    <option value="1">Kích hoạt</option>
                    </select>
                  </td>
                  <td></td>
              </tr>
              {Element}
          </tbody>
      </table>
      
    );
  }
}
const mapStateToProps=(state)=>
{
  return {
    Tasks : state.Tasks,
    Filter:state.Filter,
    keyword:state.Keyword,
    SortBy:state.SortBy
  }
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onFilterTable: (Filter) => {
      
      dispatch(action.FilterTask(Filter))
    }
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(TaskList);
