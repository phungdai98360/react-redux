import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as actions from './../action/index';
class TaskForm extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      id:'',
      name :'',
      status : false
    }
  }
  onClearForm=()=>
  {
    this.setState({
      id:'',
      name :'',
      status : false
     });
  }
  componentWillMount()
  {
    if(this.props.TaskFormUpdate && this.props.TaskFormUpdate.id!==null )
    {
      this.setState({
        id:this.props.TaskFormUpdate.id,
        name:this.props.TaskFormUpdate.name,
        status:this.props.TaskFormUpdate.status
      });
    }
    else 
    {
      this.onClearForm();
    }
  }
  componentWillReceiveProps(nextProps)
  {
    
    if(nextProps && nextProps.TaskFormUpdate)
    {
      this.setState({
        id:nextProps.TaskFormUpdate.id,
        name:nextProps.TaskFormUpdate.name,
        status:nextProps.TaskFormUpdate.status
      });
    }
    else if(nextProps && nextProps.TaskFormUpdate===null)
    {
      this.onClearForm();
    }
  }
  
  onChange=(event)=>
  {
    var target=event.target;
    var name=target.name;
    var value=target.value;
    if(name==='status')
    {
      value=target.value==='true'?true:false;
    }
    this.setState({
      [name]: value
    });
  }
  
  onCloseForm=()=>
  {
    this.props.onCloseForm();
  }
  onSubmit=(event)=>
  {
    event.preventDefault();
    //this.props.onSubmit(this.state);
    this.props.onAddTasks(this.state);
    this.onClearForm();
    this.onCloseForm();
  }
  onClearTask=()=>
  {
    this.props.onClearTask({
      id:'',
      name:'',
      status:false
    });
    this.onCloseForm();
  }
  render() {
    //var {id}=this.state;
    
    return (
      
      <div className="panel panel-warning">
            <div className="panel-heading">
                  <h3 className="panel-title">{!this.state.id?'Thêm công việc':'Cập nhật công việc'}
                   <span className="fa fa-trash text-right" onClick={this.onCloseForm}></span>
                  </h3>
            </div>
            <div className="panel-body">
                  
                  <form onSubmit={this.onSubmit}>
                      <div className="form-group">
                         <label>Tên :</label>
                         <input
                          type="text"
                          className="form-control"
                          name="name"
                          value={this.state.name}
                          onChange={this.onChange}
                         >
                         </input>
                      </div>
                      <label>Trạng thái:</label>
                      <select 
                      name="status"
                      className="form-control"
                      value={this.state.status}
                      onChange={this.onChange}
                      >
                      <option value={true}>Kích hoạt</option>
                      <option value={false}>Ẩn</option>
                      </select><br/>
                      <div className="text-center">
                        
                        <button type="submit" className="btn btn-warning">
                        <span className="fa fa-plus mr-5"></span>
                        Save
                        </button>&nbsp;
                        
                        <button type="button" className="btn btn-danger" onClick={this.onClearTask}>
                         <span className="fa fa-close mr-5"></span>
                           Cancel
                        </button>
                        
                        
                      </div>
                  </form>
                  
            </div>
      </div>
      
    );
  }
}
const mapStateToProps = (state) => {
  return {
     TaskFormUpdate : state.Editing
  }
};
const mapDispatchToProps = (dispatch, props) => {
  return {
      onAddTasks:(Task)=>
      {
        dispatch(actions.AddTasks(Task));
      },
      onCloseForm:()=>
    {
      dispatch(actions.CloseForm())
    },
    onClearTask:(Task)=>
    {
      dispatch(actions.EditTask(Task));
    }
  }
};

export default connect(mapStateToProps,mapDispatchToProps)(TaskForm);