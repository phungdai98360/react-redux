import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as action from './../action/index';

class Sort extends Component {
  onClick=(sortBy,sortValue)=>
  {
    var SortBy={
      By:sortBy,
      Value:sortValue
    };
    this.props.onSort(SortBy);
  }
  render() {
    return (
     
     <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <div className="dropdown">
            
            <button type="button"
            className="btn btn-primary dropdown-toggle"
            id="dropdownMenu1"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="true"
            >
            Sort <span className="fa fa-caret-square-o-down ml-5"/>
            </button>
            
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li onClick={()=>this.onClick('name',1)}>
                    <a role="button" className={(this.props.SortBy.By==='name'&&this.props.SortBy.Value===1)?'sort_selected':''}>
                       <span className="fa fa-sort-alpha-asc pr-5">Tên A-Z</span>
                    </a>
                </li>
                <li onClick={()=>this.onClick('name',-1)}>
                  <a role="button" className={(this.props.SortBy.By==='name'&&this.props.SortBy.Value===-1)?'sort_selected':''}>
                    <span className="fa fa-sort-alpha-desc pr-5">Tên Z-A</span>
                  </a>
                </li>
                <li role="separator" className="divider"></li>
                <li onClick={()=>this.onClick('status',1)}>
                    <a role="button" className={(this.props.SortBy.By==='status'&&this.props.SortBy.Value===1)?'sort_selected':''}>
                        <span>Trạng thái kích hoạt</span>
                    </a>
                </li>
                <li  onClick={()=>this.onClick('status',-1)}>
                    <a role="button" className={(this.props.SortBy.By==='status'&&this.props.SortBy.Value===-1)?'sort_selected':''}>
                        <span>Trạng thái ẩn</span>
                    </a>
                </li>
            </ul>
            
            
         </div>
     </div>
     
      
    );
  }
}
const mapStateToProps = state => {
  return {
    SortBy:state.SortBy
  }
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSort: (SortBy) => {
      dispatch(action.onSort(SortBy))
    }
  }
};
export default connect(mapStateToProps,mapDispatchToProps)(Sort);
