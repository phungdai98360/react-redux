import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as action from './../action/index';

class TaskItem extends Component {
  onUpdateStatus=()=>
  {
    this.props.onUpdateStatus(this.props.Task.id);
  }
  onDeleteList=()=>
  {
    this.props.onDeleteList(this.props.Task.id);
  }
  onUpdateList=()=>
  {
    //console.log(this.props.Task);
   this.props.onOpenForm();
   this.props.onUpdateList(this.props.Task);
  }
  render() {
    var {Task,index}=this.props;
    return (
      <tr>
        <td>{index+1}</td>
        <td>{Task.name}</td>
        <td className="text-center">
            <span 
            onClick={this.onUpdateStatus}
            className={Task.status===true?"label label-success":"label label-danger"}
            >
            {Task.status===true?"Kich hoạt":"Ẩn"}
            </span>
        </td>
        <td className="text-center">
            
            <button type="button" className="btn btn-warning" onClick={this.onUpdateList}><span className="fa fa-pencil mr-5">Update</span></button>
            &nbsp;
            
            <button type="button" className="btn btn-danger" onClick={this.onDeleteList}><span className="fa fa-trash mr-5">Delete</span></button>
            
            
        </td>
      </tr>
      
      
    );
  }
}
const mapStateToProps = state => {
  return {
    
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateStatus: (id) => {
      dispatch(action.UpdateStatus(id));
    },
    onDeleteList:(id)=>{
      dispatch(action.DeleteTasks(id));
    },
    onOpenForm:()=>
    {
      dispatch(action.OpenForm())
    },
    onUpdateList:(task)=>
    {
      dispatch(action.EditTask(task));
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(TaskItem);
