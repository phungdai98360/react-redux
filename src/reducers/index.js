import {combineReducers} from 'redux';
import Tasks from './Tasks';
import ToggleForm from './ToggleForm';
import Editing from './Editing';
import Filter from './Filter';
import Keyword from './Keyword';
import SortBy from './SortBy';
const myReducer = combineReducers({
        Tasks:Tasks,
        ToggleForm:ToggleForm,
        Editing:Editing,
        Filter,
        Keyword,
        SortBy
});


export default myReducer;