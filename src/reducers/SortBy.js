import * as types from './../constant/ActionType';


var initialState={
    By:'',
    Value:-1
}
var myReducer=(state=initialState, action)=>
{
    switch(action.type)
    {
        case types.SORT:
            //console.log(action);
            return{
                By:action.SortBy.By,
                Value:parseInt(action.SortBy.Value,10)
            };
        default: return state;
    }
        

};
export default myReducer;