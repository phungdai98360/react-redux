import * as types from './../constant/ActionType';
import _ from 'lodash';
var CreateString=()=>
  {
    return Math.floor((1+Math.random())*0x10000).toString(16).substring(1);
  }
  var CreateId=()=>
  {
    return CreateString() + CreateString() +'-'+CreateString() +'-'+ CreateString() +'gggg' +CreateString();
  }
var data=JSON.parse(localStorage.getItem('Tasks'));
var initialState=data?data:[

];
var myReducer=(state=initialState, action)=>
{
    switch(action.type)
    {
        case types.LIST_ALL:
            return state;
        case types.SAVE_TASKS:
          
            var newTask ={
                id:action.Task.id,
                name:action.Task.name,
                status:action.Task.status
            }
            if(!newTask.id)
            {
              newTask.id=CreateId();
              state.push(newTask);
            }
            else
            {
              var index=_.findIndex(state,(task)=>
              {
                return task.id===newTask.id;
              });
              state[index]=newTask;
            }
            
            localStorage.setItem('Tasks',JSON.stringify(state));
            return [...state]; 
        case types.UPDATE_STATUS:
              
              var id=action.id;
              var index=_.findIndex(state,(task)=>{
                  return task.id===id;
              });
                state[index]={
                  ...state[index],
                  status:!state[index].status
                };

                localStorage.setItem('Tasks',JSON.stringify(state));
              
                return [...state];
        case types.DELETE_TASKS:
            var id=action.id;
            var index=_.findIndex(state,(task)=>{
              return task.id===id;
            });
            state.splice(index,1);
            localStorage.setItem('Tasks',JSON.stringify(state));
        
            return[...state];
        default: return state;
    }
        

};
export default myReducer;