import * as types from './../constant/ActionType';
export const ListALL=()=>
{
     return {
            type:types.LIST_ALL
     }
};

export const AddTasks=(Task)=>
{
     return {
          type:types.SAVE_TASKS,
          Task
     }
     
};
export const ToggleForm=()=>
{
     return {
          type:types.TOGGLE_FORM
     }
};
export const OpenForm=()=>
{
     return {
          type:types.OPEN_FORM
     }
};
export const CloseForm=()=>
{
     return {
          type:types.CLOSE_FORM
     }
};

export const UpdateStatus=(id)=>
{
     return {
          type: types.UPDATE_STATUS,
          id
     }
};
export const DeleteTasks=(id)=>
{
     return {
          type:types.DELETE_TASKS,
          id
     }
};
export const EditTask=(Task)=>
{
     return {
          type:types.EDIT_ITEM,
          Task
     }
};
export const FilterTask=(Filter)=>
{
     return {
          type:types.FILTER_TATBLE,
          Filter
     }
};
export const Search=(Keyword)=>
{
     return {
          type:types.KEYWORD,
          Keyword
     }
};
export const onSort=(SortBy)=>
{
     return {
          type:types.SORT,
          SortBy
     }
}