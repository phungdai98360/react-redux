import React, { Component } from 'react';
import './App.css';
import TaskForm from './Components/TaskForm';
import TaskList from './Components/TaskList';
import TaskControl from './Components/TaskControl';
import {connect} from 'react-redux';
import * as action from './action/index';

//import { parse } from 'url';
class App extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      
     
     
      
      SortBy:'name',
      SortValue:1
    }
  }
  
  onTogleForm=()=>
  {
    if(this.props.Editing && this.props.Editing.id!=='')
    {
      this.props.onOpenForm();
    }
   
     else
     {
      this.props.onToggleForm();
      
     }
     this.props.onClearTask({
      id:'',
      name:'',
      status:false
    });
   
   
  }
  
  onGenerateData=()=>
  {
      var Tasks = [
        {
          id:this.CreateId(),
          name:'Học Angular7',
          status:true
        },
        {
          id:this.CreateId(),
          name:'Học react',
          status:false
        },
        {
          id:this.CreateId(),
          name:'Học Angular7',
          status:false
        },
        {
          id:this.CreateId(),
          name:'Học Angular7',
          status:true
        }
      ];
      this.setState({
        Tasks:Tasks
      });
      localStorage.setItem('Tasks',JSON.stringify(Tasks));
  }
  
 
  
   
   
  render() {
    var {keyword,SortBy,SortValue}=this.state;
    var {ToggleForm}=this.props;
    var ElementForm=ToggleForm===true ? <TaskForm 
    
    />:'';
    
    
    return (
      <div className="container">
       <div className="text-center">
         <h1>Quản lí công việc</h1>
       </div>
       <div className="row">
         
         <div className={ToggleForm? "col-xs-4 col-sm-4 col-md-4 col-lg-4":""}>
            {ElementForm}
         </div>
         
         <div className={ToggleForm===true ?"col-xs-8 col-sm-8 col-md-8 col-lg-8 ":"col-xs-12 col-sm-12 col-md-12 col-lg-12 "}>
            
            <button type="button" className="btn btn-primary" onClick={this.onTogleForm}>
            <span className="fa fa-plus mr-5"/>
            Thêm công việc
            </button>
            
            <button type="button"
             className="btn btn-danger"
             onClick={this.onGenerateData}
             >
            Generate data</button>
            <br/>
            <br/>
            
            <TaskControl/>
            <TaskList/>
         </div>
         
         
       </div>
       
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    ToggleForm : state.ToggleForm,
    Editing: state.Editing
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onToggleForm:()=>
    {
      dispatch(action.ToggleForm())
    },
    onClearTask:(Task)=>
    {
      dispatch(action.EditTask(Task));
    },
    onOpenForm:()=>
    {
      dispatch(action.OpenForm())
    },
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(App);
